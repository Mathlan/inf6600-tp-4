#include "periodicTimer.h"

#include <time.h>
#include <signal.h>
#include <sys/siginfo.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>

PeriodicTimer::~PeriodicTimer()
{
    //deleteTimer();
}

void PeriodicTimer::setupTimer(){
    channel_id = ChannelCreate(0);
    /* Set event as pulse and attach to channel */
    event.sigev_notify = SIGEV_PULSE;
    event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, channel_id,
    _NTO_SIDE_CHANNEL, 0);
    /* Set basic priority */
    event.sigev_priority = 0;
//event.sigev_code = TASK_PULSE_CODE;
    /* Create timer and associate to event */
    timer_create(CLOCK_MONOTONIC, &event, &timer); 
}

void PeriodicTimer::startTimer(const uint32_t period){
    int32_t period_s;
    int32_t period_ns;
    /* Set the itime structure */
    period_s = period / 1000000;
    period_ns = (1000 * period) - (period_s * 1000000);
    itime.it_value.tv_sec = period_s;
    itime.it_value.tv_nsec = period_ns;
    itime.it_interval.tv_sec = period_s;
    itime.it_interval.tv_nsec = period_ns;
    /* Set the timer period */
    timer_settime(timer, 0, &itime, NULL);
}

void PeriodicTimer::waitPeriod(){
    struct _pulse pulse;

    MsgReceivePulse(channel_id, &pulse, sizeof(pulse), NULL);
}

