#ifndef _PERIODIC_TIMER_H_
#define _PERIODIC_TIMER_H_

#include <iostream>
#include <time.h>
#include <signal.h>
#include <sys/siginfo.h>

class PeriodicTimer
{
public:
    PeriodicTimer() = default;

    virtual ~PeriodicTimer();

    void setupTimer();

    void startTimer(const uint32_t period);
    /* period in us */

    void waitPeriod();

private:

    timer_t timer{};
    sigevent event{};
    itimerspec itime{};
    int channel_id{};
};

#endif // _PERIODIC_TIMER_H_
