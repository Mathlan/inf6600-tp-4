#include <iostream>
#include <time.h>
#include <signal.h>
#include <sys/siginfo.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/syspage.h>
#include <sys/neutrino.h>
#include <sched.h>
#include <cmath>
#include "cameraModule.h"
#include "periodicTimer.h"

using namespace std;

//Speef of the simulation
#define SIMU_ACCEL 100

#define PI 3.141592
//Params of the simulation
#define INIT_NIV_BAT  100.
#define VITESSE_MAX_MS  5.
#define BAT_AUTONOMY_M  500.
#define TIME_TO_CHARGE_M 15.
#define BAT_AUTONOMY_S  BAT_AUTONOMY_M*60.
#define TIME_TO_CHARGE_S  TIME_TO_CHARGE_M*60.

//Camera buffer size
#define BUFFER_PIC_NB 25

//Field waypoint tab length
#define WAYPOINTS_NB 28


// Mission states
#define SCAN 0 //Scan the field (take pictures)
#define SENDING_PIC 1 //Sending the pictures
#define RTB 2 //Return above the base
#define RTW 3 //Return to work (last position before the drone came back to base)
#define CHARGE 4 //Charge at base
#define DISPLACEMENT_11 5 //Go to destination at 11m above the ground
#define DISPLACEMENT_5 6 //Go down to 5m
#define LANDING 7 //Land at base
#define FINISH 8 //The drone has finished its work
#define WAITING 9 //The drone wait

//Monitoring
struct timespec start_time;
struct timespec finish_time;
bool display = true;


//mutex
pthread_rwlock_t lock1; //current_pos
pthread_mutex_t mut2 = PTHREAD_MUTEX_INITIALIZER; //current_command
pthread_mutex_t mut3 = PTHREAD_MUTEX_INITIALIZER; //destination

// Cond var
// Arrived to dest
pthread_mutex_t cdmut1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER;
bool arrived = false;
// Take photo
pthread_mutex_t cdmut2 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond2 = PTHREAD_COND_INITIALIZER;
bool take_pic = false;
//Transmit photos
pthread_mutex_t cdmut3 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond3 = PTHREAD_COND_INITIALIZER;
bool transmit_pic = false;
//Transmission confirmed
pthread_mutex_t cdmut4 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond4 = PTHREAD_COND_INITIALIZER;
bool transmition_confirmed = false;
// Camera available
pthread_mutex_t cdmut5 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond5 = PTHREAD_COND_INITIALIZER;
bool camera_available = true;


// Useful strucs
typedef struct
{
    double x;
    double y;
    double z;
} position;

typedef struct
{
   double orientation;
   double horizontal_speed;
   double vertical_speed;
} command;

//Useful functions
void pos2coord(position* position, coord_t* coord){
	coord->x=position->x;
	coord->y=position->y;
}

double dist2D(position* p1, position* p2) {
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    return sqrt(pow((x1-x2),2) + pow((y1-y2),2));
}

double dist3D(position* p1, position* p2){
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    double z1 = p1->z;
    double z2 = p2->z;
    return sqrt(pow((x1-x2),2)+pow((y1-y2),2)+pow((z1-z2),2));
}

double distZ(position* p, position* target){
    return target->z-p->z;
}

void set_position(position* destination, position* target){
    destination->x = target->x;
    destination->y = target->y;
    destination->z = target->z;
}



// Variables used in the controler
position* destination; //Destination of the drone
position* current_pos; //Current position of the drone
command* current_command; //Current navigation command send to the hardware
double current_orientation;
double last_orientation_command; //Last orientation command of the drone (used to calculate the current orientation command)
double target_speed; //Targeted horizontal speed (1m/s when scanning / 10m/s ortherwise)
position** waypoints; //Waypoints to explore the field
int waypoint_index; //Index in the waypoint tab of the current waypoint
int mission_state; //State of the mission controler
int last_mission_state; //State of the mission controlleur before an interruption (sending pictures)
position** base_acces_positions; //Positions used to reach the base (0 : base / 1 : upward)
position* return_position; //Position to reach after a low bat alarm an charge at base
int buffer; //Picture buffer (number of pictures stored)
bool need_resend; //Used to resent pictures if transmission failed (ex : low bat during transmission)
int is_pic_taken; //input of the picture confirmation from the hardware
bool last_pic_confirmed; //Used to be sure that the last picture was taken
position* pos_last_picture; //Position of the drone when the last picture was taken
double battery_level; //battery level
double current_hspeed; // vitesse horizontale actuelle
double current_vspeed; // vitesse verticale actuelle
bool charge_battery; // consigne de charge de la batterie
bool waypoint_photo; // Demande de prise de photo à un waypoint

// Camera
PathMap camera;
coord_t* coord_photo = new coord_t;

//Timers
PeriodicTimer simulation_timer;
PeriodicTimer nav_controller_timer;
PeriodicTimer cam_controller_timer;

// Task that simulate the displacement of the drone and battery consumption
void *Displacement_battery_simulation(void *arg){

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x02) == -1) {
  	printf("ERROR 0\n");
  	}

	bool low_bat = false;
	double delta_t;
	struct timespec last_time;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &last_time);

	simulation_timer.startTimer(10000/SIMU_ACCEL);

	while(1){
		pthread_rwlock_wrlock(&lock1);
		pthread_mutex_lock(&mut2);
		clock_gettime(CLOCK_REALTIME, &current_time);
		delta_t = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_time.tv_sec-0.000000001*last_time.tv_nsec)*SIMU_ACCEL;

		//printf("%lf\n",delta_t);

		//Update position and orientation
		current_hspeed = min(VITESSE_MAX_MS,current_command->horizontal_speed);
		current_vspeed = current_command->vertical_speed;
		current_orientation = current_command->orientation;
		current_pos->x += sin(current_orientation*PI/180.)*current_hspeed*delta_t;
		current_pos->y += cos(current_orientation*PI/180.)*current_hspeed*delta_t;
		current_pos->z = max(0.,current_pos->z+current_vspeed*delta_t);

		//Update battery
		if (charge_battery){
			battery_level += (80./(TIME_TO_CHARGE_S))*delta_t;
			if (battery_level>=100){ //Full battery signal
				raise(SIGUSR2);
				low_bat=false;
			}
		}
		else{
			battery_level -= (100./BAT_AUTONOMY_M)*(current_vspeed+current_hspeed)*delta_t;
		}
		battery_level = max(0.,battery_level);
		battery_level = min(100.,battery_level);
		if(battery_level<=10 && !low_bat){ //Low battery signal
			raise(SIGUSR1);
			low_bat = true;
		}
		last_time = current_time;
		pthread_mutex_unlock(&mut2);
		pthread_rwlock_unlock(&lock1);

		simulation_timer.waitPeriod();
	}
	return(NULL);
}

// Task that simulate the camera
void *Camera_simulation(void *arg){

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x02) == -1) {
  	printf("ERROR 7\n");
  	}

	while(1){
		//Wait signal to take a photo
		pthread_mutex_lock(&cdmut2);
		while(!take_pic){
			pthread_cond_wait(&cond2, &cdmut2);
		}
		take_pic = false;
		pthread_mutex_unlock(&cdmut2);
		//Take photo
		is_pic_taken = 0;
		pthread_rwlock_rdlock(&lock1);
		pos2coord(current_pos,coord_photo);
		pthread_rwlock_unlock(&lock1);
		camera.takePhoto(*coord_photo, SIMU_ACCEL);
		//Indicate picture taken
		is_pic_taken = 1;
	}
}

//Task that simulate pictures transmission
void *Transmission_simulation(void *arg){

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x02) == -1) {
  	printf("ERROR 6\n");
  	}

	while(1){
		//Wait signal to transmit a photos
		pthread_mutex_lock(&cdmut3);
		while(!transmit_pic){
			pthread_cond_wait(&cond3, &cdmut3);
		}
		transmit_pic = false;
		pthread_mutex_unlock(&cdmut3);
		//Transmit photos
		camera.transmitPhotos(SIMU_ACCEL);
		//Signal transmission finished
		pthread_mutex_lock(&cdmut4);
		transmition_confirmed = true;
		pthread_cond_signal(&cond4);
		pthread_mutex_unlock(&cdmut4);
	}
}

//Task to monitor the simulation
void *Display(void *arg){

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x02) == -1) {
  	printf("ERROR 1\n");
  	}

	while(1){
		if(display){
			pthread_rwlock_rdlock(&lock1);
			switch (mission_state){
				case SCAN :
					printf("State : SCAN\n");
					break;
				case SENDING_PIC :
					printf("State : SENDING_PIC\n");
					break;
			 	case RTB :
			 		printf("State : RTB\n");
			 		break;
			 	case RTW :
			 		printf("State : RTW\n");
			 		break;
			 	case CHARGE :
			 		printf("State : CHARGE\n");
			 		break;
			 	case DISPLACEMENT_11 :
			 		printf("State : DISPLACEMENT_11\n");
			 		break;
			 	case DISPLACEMENT_5 :
			 		printf("State : DISPLACEMENT_5\n");
			 		break;
	 			case LANDING :
	 				printf("State : LANDING\n");
	 				break;
	 			case FINISH :
	 				printf("State : FINISH\n");
	 				display = false;
	 				break;
	 			case WAITING :
	 				printf("State : WAITING\n");
	 				break;
			}
			printf("x : %lf / y : %lf / z : %lf\n",current_pos->x,current_pos->y,current_pos->z);
			printf("orientation : %lf\n",current_orientation);
			printf("h_speed : %lf / v_speed : %lf\n",current_hspeed,current_vspeed);
			printf("battery level : %lf\n",battery_level);
			printf("buffer : %i\n\n",buffer);


			pthread_rwlock_unlock(&lock1);
			sleep(1);
		}
	}
	return(NULL);
}


//Navigation controler task
void * nav_ctrl_func(void* arg) {

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x01) == -1) {
  	printf("ERROR 0\n");
  	}

  	/*//Monitoring
  	double delta_t;
	struct timespec last_time;
	struct timespec current_time;
	clock_gettime(CLOCK_REALTIME, &last_time);*/

	nav_controller_timer.startTimer(100000/SIMU_ACCEL);

	while(1){
		pthread_rwlock_rdlock(&lock1);
		pthread_mutex_lock(&mut2);

		/*//Monitoring
		clock_gettime(CLOCK_REALTIME, &current_time);
		delta_t = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_time.tv_sec-0.000000001*last_time.tv_nsec)*SIMU_ACCEL;
		printf("%lf\n",delta_t);
		last_time = current_time;*/

		//Special state commands
		if (mission_state == WAITING){
			current_command->horizontal_speed =0;
			current_command->vertical_speed = 0;
		}
		else if (mission_state==SENDING_PIC && last_mission_state != FINISH){
			current_command->vertical_speed=0;
			if (dist2D(pos_last_picture,current_pos)>5.05){
					pthread_mutex_lock(&mut3);
					current_command->orientation = atan2(destination->x-current_pos->x,destination->y-current_pos->y)*180./PI;
					pthread_mutex_unlock(&mut3);
					current_command->orientation = current_command->orientation+180.;
					current_command->horizontal_speed=0.1;
			}
			else{
					current_command->horizontal_speed=0;
			}

		}
		//Usual state command
		else if (mission_state != CHARGE && mission_state!=FINISH){

			pthread_mutex_lock(&mut3);
			current_command->orientation = atan2(destination->x-current_pos->x,destination->y-current_pos->y)*180./PI;
			pthread_mutex_unlock(&mut3);

			if (abs(current_command->orientation+360.-last_orientation_command)<abs(current_command->orientation-last_orientation_command)){
				current_command->orientation = current_command->orientation +360.;
			}

			last_orientation_command = current_command->orientation;

			pthread_mutex_lock(&mut3);
			if (dist2D(current_pos,destination)>0.05){
				pthread_mutex_lock(&mut3);
				current_command->horizontal_speed = min(target_speed,dist2D(current_pos,destination));
				pthread_mutex_unlock(&mut3);
			}
			else{
				current_command->horizontal_speed = 0;
			}

			if (abs(distZ(current_pos, destination))>0.05){
				if(distZ(current_pos, destination)>0){
					current_command->vertical_speed = min(1.,distZ(current_pos, destination));
				}
				else{
					current_command->vertical_speed = max(-1.,distZ(current_pos, destination));
				}
			}
			else{
				current_command->vertical_speed = 0;
			}
			pthread_mutex_unlock(&mut3);
		}

		//Check if arrived at destination
		if (mission_state != CHARGE && mission_state != SENDING_PIC && mission_state!=FINISH && mission_state!=WAITING){
			//We arrived to the destination
			pthread_mutex_lock(&mut3);
			if (dist3D(current_pos,destination) < 0.1) {
				//printf("Arrived at destination\n");
				current_command->horizontal_speed=0;
				current_command->vertical_speed=0;
				//Signal arrived at destination
				pthread_mutex_lock(&cdmut1);
				arrived = true;
				pthread_cond_signal(&cond1);
				pthread_mutex_unlock(&cdmut1);
			}
			pthread_mutex_unlock(&mut3);
		}

		pthread_mutex_unlock(&mut2);
		pthread_rwlock_unlock(&lock1);

		nav_controller_timer.waitPeriod();
	}
}

//Reached destination handler
void* arrived_to_dest(void*arg) {

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x01) == -1) {
  	printf("ERROR 3\n");
  	}

	while(1){
		//Wait arrived at destination signal
		pthread_mutex_lock(&cdmut1);
		while(!arrived){
			pthread_cond_wait(&cond1, &cdmut1);
		}
		arrived = false;
		pthread_mutex_unlock(&cdmut1);

		//Set next destination / state
		switch (mission_state){
		    case RTW:
		    	pthread_mutex_lock(&mut3);
		        set_position(destination,return_position);
		        destination->z=11;
		        mission_state = DISPLACEMENT_11;
		        target_speed = 10;
		        pthread_mutex_unlock(&mut3);
		        //printf("DISPLACEMENT_11\n");
		        break;

		    case DISPLACEMENT_11:
		    	pthread_mutex_lock(&mut3);
		        destination->z=5;
		        mission_state = DISPLACEMENT_5;
		        pthread_mutex_unlock(&mut3);
		        //printf("DISPLACEMENT_5\n");
		        break;

		    case DISPLACEMENT_5:
		    	pthread_mutex_lock(&mut3);
		        set_position(destination,waypoints[waypoint_index]);
		        mission_state=SCAN;
		        target_speed = 1;
		        pthread_mutex_unlock(&mut3);
		        //printf("SCAN\n");
		        break;

		    case SCAN:
		    	//Try to take photo at waypoint
		    	if (!camera_available){
		    		//printf("\nwaiting camera\n\n");
		    		mission_state = WAITING;
		    		//Wait camera available signal
		    		pthread_mutex_lock(&cdmut5);
					while(!camera_available){
						pthread_cond_wait(&cond5, &cdmut5);
					}
					pthread_mutex_unlock(&cdmut5);
		    	}

		    	//Safety
		    	if (mission_state == WAITING){
		    		mission_state = SCAN;
		    	}
		    	//Set order to take photo
		    	waypoint_photo=true;
		    	pthread_mutex_lock(&cdmut5);
		    	//Wait order received by cam controler
				while(camera_available){
					pthread_cond_wait(&cond5, &cdmut5);
				}
				pthread_mutex_unlock(&cdmut5);
				//Set next waypoint
		        waypoint_index++;
		        if (waypoint_index < WAYPOINTS_NB) {
		             //the Mission controler gives the new point to the Nav controler
		        	pthread_mutex_lock(&mut3);
		            set_position(destination,waypoints[waypoint_index]);
		            pthread_mutex_unlock(&mut3);
		            //printf("Next waypoint\n");
		        }
		        else {
		            //Return to the base
		            //printf("All points have been reached\n");
		            pthread_mutex_lock(&mut3);
		            set_position(destination,base_acces_positions[1]);
		            mission_state = RTB;
		            target_speed = 10;
		            pthread_mutex_unlock(&mut3);
		            //printf("RTB\n");
		        }
		        break;

		    case RTB:
		    	pthread_mutex_lock(&mut3);
		        set_position(destination,base_acces_positions[0]);
		        mission_state = LANDING;
		        pthread_mutex_unlock(&mut3);
		        //printf("LANDING\n");
		        break;

		    case LANDING:
		        //printf("CHARGE\n");
		        charge_battery = true;

		        if (waypoint_index < WAYPOINTS_NB) {
		            if(need_resend){
		                last_mission_state = CHARGE;
		                mission_state = SENDING_PIC;
		                printf("Resend\n");
		            }
		            else{
		                mission_state = CHARGE;
		            }
		        }
		        //Mission finished
		        else {
		          	mission_state = FINISH;
		          	target_speed = 0;
		          	//in case pictures are in the buffer at the end of the mission
		          	if (buffer > 0) {
                  		last_mission_state = FINISH;
                  		mission_state = SENDING_PIC;
                  		pthread_mutex_lock(&cdmut3);
						transmit_pic = true;
						pthread_cond_signal(&cond3);
						pthread_mutex_unlock(&cdmut3);

		            	printf("Sending last pictures remaining\n");
		          	}
		        }
		}
		//Safety
		pthread_mutex_lock(&cdmut1);
		arrived = false;
		pthread_mutex_unlock(&cdmut1);
	}
}


//Camera controler task
void *cam_ctrl_func(void* arg) {

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x01) == -1) {
  	printf("ERROR 5\n");
  	}

/*  	//Monitoring
  	double delta_t;
	struct timespec last_time;
	clock_gettime(CLOCK_REALTIME, &last_time);*/

	struct timespec last_shot_time;
	struct timespec current_time;
	double shooting_time;

	cam_controller_timer.startTimer(100000/SIMU_ACCEL);

	while(1){
		clock_gettime(CLOCK_REALTIME, &current_time);
		shooting_time = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_shot_time.tv_sec-0.000000001*last_shot_time.tv_nsec)*SIMU_ACCEL;


/*		//Monitoring
		delta_t = (current_time.tv_sec+0.000000001*current_time.tv_nsec-last_time.tv_sec-0.000000001*last_time.tv_nsec)*SIMU_ACCEL;
		printf("%lf\n",delta_t);
		last_time = current_time;*/


		if (!camera_available || !last_pic_confirmed){
	        if(is_pic_taken==1){
	        	//Signal camera available
	        	pthread_mutex_lock(&cdmut5);
				camera_available = true;
				pthread_cond_signal(&cond5);
				pthread_mutex_unlock(&cdmut5);
				//Update buffer and confirm picture taken
	            buffer++;
	            last_pic_confirmed=true;
	            //Transmit pictures if full buffer
	            if (buffer == BUFFER_PIC_NB){
	                last_mission_state = mission_state;
	                mission_state=SENDING_PIC;
	                //printf("SENDING_PIC\n");
	                pthread_mutex_lock(&cdmut3);
					transmit_pic = true;
					pthread_cond_signal(&cond3);
					pthread_mutex_unlock(&cdmut3);
            	}
        	}
        	else if (shooting_time > 4.5 && !camera_available){
        		printf("Error : picture not taken\n");
        		pthread_mutex_lock(&cdmut5);
				camera_available = true;
				pthread_cond_signal(&cond5);
				pthread_mutex_unlock(&cdmut5);
        	}
    	}
	    if(mission_state == SCAN || waypoint_photo){
	    	//Take a picture at waypoint
	        if (waypoint_photo){
	        	//printf("Picture at waypoint\n\n");
	            clock_gettime(CLOCK_REALTIME, &last_shot_time);
	            pthread_rwlock_rdlock(&lock1);
	            set_position(pos_last_picture,current_pos);
	            pthread_rwlock_unlock(&lock1);
	            last_pic_confirmed=false;
	            waypoint_photo=false;
	            
	            //Signal to take a picture
	            pthread_mutex_lock(&cdmut2);
				take_pic = true;
				pthread_cond_signal(&cond2);
				pthread_mutex_unlock(&cdmut2);

				//Signal camera is not available
				pthread_mutex_lock(&cdmut5);
				camera_available=false;
				pthread_cond_signal(&cond5);
				pthread_mutex_unlock(&cdmut5);

	        }
	        //Take pictures every 5m
	        else{
	        	pthread_rwlock_rdlock(&lock1);
	        	if (dist2D(pos_last_picture,current_pos)>4.9){
	            	//printf("Picture distance : %lf\n\n",dist2D(pos_last_picture,current_pos));
		            set_position(pos_last_picture,current_pos);
		            last_pic_confirmed=false;
		            //Signal camera is not available
		            pthread_mutex_lock(&cdmut5);
					camera_available=false;
					pthread_cond_signal(&cond5);
					pthread_mutex_unlock(&cdmut5);

		            clock_gettime(CLOCK_REALTIME, &last_shot_time);
		            //Signal to take picture
		            pthread_mutex_lock(&cdmut2);
					take_pic = true;
					pthread_cond_signal(&cond2);
					pthread_mutex_unlock(&cdmut2);
		        }
	        	pthread_rwlock_unlock(&lock1);
	    	}
	    }

		cam_controller_timer.waitPeriod();
	}
}

//Low battery alarm handler
void batt_low_alarm(int signum){
	printf("LOW_BAT\n");
	//Set resend pictures 
    if (mission_state==SENDING_PIC){
        need_resend=true;
    }
    //Set return position
    pthread_rwlock_rdlock(&lock1);
    set_position(return_position,current_pos);
    pthread_rwlock_unlock(&lock1);
    //Set base destination
    pthread_mutex_lock(&mut3);
    set_position(destination,base_acces_positions[1]);
    target_speed = 10;
    mission_state = RTB;
    pthread_mutex_unlock(&mut3);
}



//Full battery alarm handler
void batt_full_alarm(int signum){
	printf("FULL_BAT\n");
    charge_battery = false;
    //Set return base exit destination
    if (mission_state == CHARGE){
    	pthread_mutex_lock(&mut3);
        set_position(destination,base_acces_positions[1]);
        mission_state = RTW;
        pthread_mutex_unlock(&mut3);
    }
}


//Transmission confirmed handler
void *pic_sent(void* arg) {

	//Runmask
  	if (ThreadCtl( _NTO_TCTL_RUNMASK, (void *)0x01) == -1) {
  	printf("ERROR 4\n");
  	}

    while(1){
    	//Wait signal transmission finished
    	pthread_mutex_lock(&cdmut4);
		while(!transmition_confirmed){
			pthread_cond_wait(&cond4, &cdmut4);
		}
		transmition_confirmed = false;
		pthread_mutex_unlock(&cdmut4);
		//Reset buffer and set mission state
    	if(mission_state==SENDING_PIC){
        buffer = 0;
        if (last_mission_state == WAITING){
        	mission_state = SCAN;
        }
        else{
        	mission_state=last_mission_state;
        }
        printf("PICS SENT\n");
    	}
    	//Missio is finished
    	if(last_mission_state==FINISH) {
          	mission_state = FINISH;
      		buffer = 0;
      		printf("Last pics sent\n");
      		clock_gettime(CLOCK_REALTIME, &finish_time);
      		printf("\n\n!!!!!!!!!!!\n %lf \n!!!!!!!!!!!\n\n",(finish_time.tv_sec+0.000000001*finish_time.tv_nsec-start_time.tv_sec-0.000000001*start_time.tv_nsec));
    	}
    }
}

// Init function
void init_tasks(void) {

	//Init Timers
	simulation_timer.setupTimer();
	nav_controller_timer.setupTimer();
	cam_controller_timer.setupTimer();


	//Init rw lock
	pthread_rwlock_init (&lock1,NULL);

	//Init threads
	pthread_t tid[8];
	pthread_attr_t attrib;
	int i;
	// FIFO
	struct sched_param mySchedParam;
	pthread_attr_setschedpolicy (&attrib, SCHED_FIFO);

	//Init handler signaux
	signal(SIGUSR1,batt_low_alarm);
	signal(SIGUSR2,batt_full_alarm);

	//Thread creation
	pthread_attr_init (&attrib);

	mySchedParam.sched_priority = 20;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[0], &attrib, Displacement_battery_simulation, NULL ) < 0)
		printf("taskSpawn Displacement_battery_simulation failed!\n");
	pthread_setname_np(tid[0],"Bat & cinematic simulation");


	mySchedParam.sched_priority = 1;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[1], &attrib, Display, NULL ) < 0)
		printf("taskSpawn Display failed!\n");
	pthread_setname_np(tid[1],"Display");


	mySchedParam.sched_priority = 11;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[2], &attrib, nav_ctrl_func, NULL ) < 0)
  		printf("Echec thread nav_ctrl_func\n");
  	pthread_setname_np(tid[2],"nav ctrl");


	mySchedParam.sched_priority = 11;
	pthread_attr_setschedparam(&attrib, &mySchedParam);
	if ( pthread_create(&tid[3], &attrib, arrived_to_dest, NULL ) < 0)
		printf("Echec thread arrived_to_dest\n");
	pthread_setname_np(tid[3],"destination handler");


	mySchedParam.sched_priority = 16;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[4], &attrib, pic_sent, NULL ) < 0)
  		printf("Echec thread pic_sent\n");
  	pthread_setname_np(tid[4],"finish transmission handler");


	mySchedParam.sched_priority = 13;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
 	if ( pthread_create(&tid[5], &attrib, cam_ctrl_func, NULL ) < 0)
  		printf("Echec thread cam_ctrl_func\n");
  	pthread_setname_np(tid[5],"cam ctrl");


	mySchedParam.sched_priority = 15;
  	pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[6], &attrib, Transmission_simulation, NULL ) < 0)
  		printf("Echec thread transmission\n");
  	pthread_setname_np(tid[6],"Transmission simulation");


	mySchedParam.sched_priority = 15;
 	 pthread_attr_setschedparam(&attrib, &mySchedParam);
  	if ( pthread_create(&tid[7], &attrib, Camera_simulation, NULL ) < 0)
  		printf("Echec thread camera\n");
  	pthread_setname_np(tid[7],"Camera simulation");


  	clock_gettime(CLOCK_REALTIME, &start_time);

  	//Wait end of each threads
	for (i=0; i < 8; i++)
		pthread_join(tid[i], NULL);

}



//Main thread
int main() {

//Init variables
	//Battery
	charge_battery = false;
	battery_level = INIT_NIV_BAT;

	//Init Camera buffer
    buffer = 0;

    //Init picture variables
    last_pic_confirmed = true;
    need_resend = false;
	pos_last_picture = new position;
	waypoint_photo = false;

    //Init command
    current_command = new command;
    last_orientation_command =0;

    //Init speed
    target_speed = 1;

    //Init state
    mission_state=RTW;

    //Start position
    current_pos = new position;
    current_pos->x=0;
    current_pos->y=0;
    current_pos->z=0;

    //Base access
    base_acces_positions = new position*[2];
    position* base = new position;
    position* upward = new position;
    base->x = 0;
    base->y = 0;
    base->z = 0;
    upward->x = 0;
    upward->y = 0;
    upward->z = 11;
    base_acces_positions[0]=base;
    base_acces_positions[1]=upward;

    //Init destination
    destination = new position;
    set_position(destination,upward);

//Init Field Waypoints
    waypoint_index = 1;

    position* pos1 = new position;
    pos1->x = 20.5;
    pos1->y = 77.5;
    pos1->z = 5;

    position* pos2 = new position;
    pos2->x = 43.5;
    pos2->y = 77.5;
    pos2->z = 5;

    position* pos3 = new position;
    pos3->x = 43.5;
    pos3->y = 72.5;
    pos3->z = 5;

    position* pos4 = new position;
    pos4->x = 20.5;
    pos4->y = 72.5;
    pos4->z = 5;

    position* pos5 = new position;
    pos5->x = 20.5;
    pos5->y = 67.5;
    pos5->z = 5;

    position* pos6 = new position;
    pos6->x = 43.5;
    pos6->y = 67.5;
    pos6->z = 5;

    position* pos7 = new position;
    pos7->x = 43.5;
    pos7->y = 62.5;
    pos7->z = 5;

    position* pos8 = new position;
    pos8->x = 20.5;
    pos8->y = 62.5;
    pos8->z = 5;

    position* pos9 = new position;
    pos9->x = 20.5;
    pos9->y = 57.5;
    pos9->z = 5;

    position* pos10 = new position;
    pos10->x = 43.5;
    pos10->y = 57.5;
    pos10->z = 5;

    position* pos11 = new position;
    pos11->x = 43.5;
    pos11->y = 52.5;
    pos11->z = 5;

    position* pos12 = new position;
    pos12->x = 20.5;
    pos12->y = 52.5;
    pos12->z = 5;

    position* pos13 = new position;
    pos13->x = 12.5;
    pos13->y = 47.5;
    pos13->z = 5;

    position* pos14 = new position;
    pos14->x = 57.5;
    pos14->y = 47.5;
    pos14->z = 5;

    position* pos15 = new position;
    pos15->x = 57.5;
    pos15->y = 42.5;
    pos15->z = 5;

    position* pos16 = new position;
    pos16->x = 12.5;
    pos16->y = 42.5;
    pos16->z = 5;

    position* pos17 = new position;
    pos17->x = 12.5;
    pos17->y = 37.5;
    pos17->z = 5;

    position* pos18 = new position;
    pos18->x = 57.5;
    pos18->y = 37.5;
    pos18->z = 5;

    position* pos19 = new position;
    pos19->x = 57.5;
    pos19->y = 32.5;
    pos19->z = 5;

    position* pos20 = new position;
    pos20->x = 12.5;
    pos20->y = 32.5;
    pos20->z = 5;

    position* pos21 = new position;
    pos21->x = 12.5;
    pos21->y = 27.5;
    pos21->z = 5;

    position* pos22 = new position;
    pos22->x = 57.5;
    pos22->y = 27.5;
    pos22->z = 5;

    position* pos23 = new position;
    pos23->x = 57.5;
    pos23->y = 22.5;
    pos23->z = 5;

    position* pos24 = new position;
    pos24->x = 12.5;
    pos24->y = 22.5;
    pos24->z = 5;

    position* pos25 = new position;
    pos25->x = 12.5;
    pos25->y = 17.5;
    pos25->z = 5;

    position* pos26 = new position;
    pos26->x = 57.5;
    pos26->y = 17.5;
    pos26->z = 5;

    position* pos27 = new position;
    pos27->x = 57.5;
    pos27->y = 12.5;
    pos27->z = 5;

    position* pos28 = new position;
    pos28->x = 12.5;
    pos28->y = 12.5;
    pos28->z = 5;

    waypoints = new position*[WAYPOINTS_NB];

    waypoints[0]=pos1;
    waypoints[1]=pos2;
    waypoints[2]=pos3;
    waypoints[3]=pos4;
    waypoints[4]=pos5;
    waypoints[5]=pos6;
    waypoints[6]=pos7;
    waypoints[7]=pos8;
    waypoints[8]=pos9;
    waypoints[9]=pos10;
    waypoints[10]=pos11;
    waypoints[11]=pos12;
    waypoints[12]=pos13;
    waypoints[13]=pos14;
    waypoints[14]=pos15;
    waypoints[15]=pos16;
    waypoints[16]=pos17;
    waypoints[17]=pos18;
    waypoints[18]=pos19;
    waypoints[19]=pos20;
    waypoints[20]=pos21;
    waypoints[21]=pos22;
    waypoints[22]=pos23;
    waypoints[23]=pos24;
    waypoints[24]=pos25;
    waypoints[25]=pos26;
    waypoints[26]=pos27;
    waypoints[27]=pos28;

    //Init return position
    return_position = new position;
    set_position(return_position,pos1);

    //Init threads
	init_tasks();

	return 0;
}
